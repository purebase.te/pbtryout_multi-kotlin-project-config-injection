plugins {
    kotlin("jvm")
}

dependencies {
    implementation(project(":core"))
    implementation(kotlin("stdlib"))
}

// https://stackoverflow.com/questions/48553029/how-do-i-overwrite-a-task-in-gradle-kotlin-dsl
// https://github.com/gradle/kotlin-dsl/issues/705
// https://github.com/gradle/kotlin-dsl/issues/716
val fatJar = task("fatJar", type = org.gradle.jvm.tasks.Jar::class) {
    baseName = "${project.name}-fat"
    // manifest Main-Class attribute is optional.
    // (Used only to provide default main class for executable jar)
    manifest {
        attributes["Main-Class"] = "de.purebase.app.Main"
    }

    // Note that in Gradle 5 you will have to replace configurations.runtime.map with configurations.runtime.get().map to avoid unresolved reference: isDirectory
    // https://stackoverflow.com/questions/41794914/how-to-create-the-fat-jar-with-gradle-kotlin-script/43998029
    // https://github.com/gradle/kotlin-dsl-samples/issues/1082#issuecomment-433037363
    // from(configurations.runtime.map(
    from(configurations.runtime.get().map {
        if (it.isDirectory) it else zipTree(it)
    })
    with(tasks["jar"] as CopySpec)
}

tasks {
    "build" {
        dependsOn(fatJar)
    }
}


