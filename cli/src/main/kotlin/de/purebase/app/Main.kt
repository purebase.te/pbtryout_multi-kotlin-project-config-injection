@file:JvmName("Main")
package de.purebase.app

import de.purebase.util.PhotoFileCopyHelper
import java.io.File

fun main(vararg args: String) {
    val inputDir = File(args[0])
    val outputDir = File(args[1])

    PhotoFileCopyHelper.movePhotoBackToArchivFolders(inputDir, outputDir)
}
