package de.purebase.util

import com.drew.imaging.ImageMetadataReader
import com.drew.imaging.ImageProcessingException
import com.drew.metadata.Metadata
import java.io.File
import java.io.IOException

object MetaDataHelper {

    const val EMPTY_SPACE = " "
    const val KEYWORDS = "Subject"
    const val SHUTTERSPEED = "Shutter Speed Value"
    const val APERTURE = "Aperture Value"

    fun getExifMetaData(file: File?): Map<String?, String?>? {
        var result: Map<String?, String?>? = null
        try {
            var metadata: Metadata? = null
            try {
                metadata = ImageMetadataReader.readMetadata(file)
                result = getAsMap(metadata)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        } catch (e: ImageProcessingException) {
            e.printStackTrace()
        }
        return result
    }

    private fun getAsMap(metadata: Metadata?): Map<String?, String?>? {
        val result: MutableMap<String?, String?> = HashMap()
        for (directory in metadata!!.directories) {
            for (tag in directory.tags) {
                if (tag.tagName == KEYWORDS) {
                    //linkWithKeywords(tag.getDescription());
                    result[KEYWORDS] = tag.description
                }
                if (tag.tagName == SHUTTERSPEED) {
                    result[SHUTTERSPEED] = tag.description
                }
                if (tag.tagName == APERTURE) {
                    result[APERTURE] = tag.description
                }
            }
        }
        return result
    }


}