package de.purebase.util

import java.io.File
import java.time.DateTimeException
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

object PhotoFileCopyHelper {

    private fun sortedByValueForFirstPassAllFiles(currentFile: File) = currentFile.isDirectory

    val fileWalkErrorList = ArrayList<String>()

    fun movePhotoBackToArchivFolders(sourceDir: File, archiveRootDir: File, onlyCopy: Boolean = false):List<String> {
        fileWalkErrorList.clear()
        if (sourceDir.isDirectory) {
            sourceDir
                .walk(FileWalkDirection.BOTTOM_UP)
                .sortedBy { sortedByValueForFirstPassAllFiles(currentFile = it) }
                .forEach { sourceFile ->
                    if (!sourceFile.isDirectory) {
                        try {
                            moveOrCopyFileToArchivDateFolder(photoFile = sourceFile, archiveRootDir = archiveRootDir)
                        } catch (e: Exception) {
                            fileWalkErrorList.add(e.message.toString())
                        }
                    } else {
                        if (!onlyCopy) deleteSubDirectory(sourceSubDir = sourceFile, sourceRootDir = sourceDir)
                    }
                }
            return fileWalkErrorList
        } else {
            return listOf("The sourceDir '${sourceDir.absolutePath}' isn't a directory!")
        }
    }

    @Throws(Exception::class)
    private fun deleteSubDirectory(sourceSubDir: File, sourceRootDir: File) {
        if (!sourceSubDir.path.equals(sourceRootDir.path)) {
            sourceSubDir.delete()
        }
    }

    @Throws(Exception::class)
    private fun moveOrCopyFileToArchivDateFolder(photoFile: File, archiveRootDir: File, move: Boolean = false) {

        val destinationFolder:File = generateDestinationSubFoldersPath(photoFile, archiveRootDir)
        if (!destinationFolder.exists()) throw Exception("!destinationFolder.exists()")

        try {
            photoFile.let { sourceFile ->
                if (destinationFolder != null) {
                    sourceFile.copyTo(destinationFolder)
                    if (move) sourceFile.delete()
                }
            }
        } catch (fae: FileAlreadyExistsException) {
            throw Exception("WARN exists -> " + fae.message)
        }
    }

    @Throws(Exception::class)
    private fun generateDestinationSubFoldersPath(sourceFile: File, outputDir: File): File {
        val dateFromFile = parseDateFromArchivFilename(sourceFile)
        val photoDatePath = generateDateBasedArchivFolderPathes(dateFromFile)
        val destinationFolder = File("$outputDir/${photoDatePath}/${sourceFile.name}")
        return destinationFolder
    }

    private fun generateDateBasedArchivFolderPathes(dateFromFile: LocalDate?): String {
        val photoDatePath = when {
            dateFromFile != null -> {
                dateFromFile.format(DateTimeFormatter.ofPattern("yyyy")) + "/" +
                        dateFromFile.format(DateTimeFormatter.ofPattern("yyyy-MM")) + "/" +
                        dateFromFile.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
            }
            else -> {
                "_unknown_filename_date"
            }
        }
        return photoDatePath
    }

    @Throws(DateTimeParseException::class)
    private fun parseDateFromArchivFilename(it: File): LocalDate? {
        val fileDatePart = it.nameWithoutExtension.split("-")[0]
        val dateFromFile:LocalDate = LocalDate.parse(fileDatePart, DateTimeFormatter.ofPattern("yyyyMMdd"))
        
        return dateFromFile
    }

}