package de.purebase.util

import java.io.File

object PhotoFileAnalyseHelper {

    fun extractPhotoMetadata(sourceDir: File) {
        if (sourceDir.isDirectory) {
            sourceDir.walk(FileWalkDirection.BOTTOM_UP)
                    .sortedBy { it -> sortedByValueForFirstPassAllFiles(currentFile = it) }
                    .forEach { sourceFile ->
                        if (!sourceFile.isDirectory) {
                            MetaDataHelper.getExifMetaData(sourceFile)
                        } else {
                            // Write json-file
                        }
                    }
        }
    }

    private fun sortedByValueForFirstPassAllFiles(currentFile: File) = currentFile.isDirectory
}