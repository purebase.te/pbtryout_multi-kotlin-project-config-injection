plugins {
    kotlin("jvm")
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation("com.drewnoakes:metadata-extractor:2.10.1")
}
