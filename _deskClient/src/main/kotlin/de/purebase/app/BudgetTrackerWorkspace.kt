package de.purebase.app

import de.purebase.app.view.MoveBackToCatalogEditor
import javafx.scene.control.TabPane
import tornadofx.*


class BudgetTrackerWorkspace : Workspace("Budget Tracker Workspace", NavigationMode.Tabs) {

    init {
        tabContainer.tabClosingPolicy = TabPane.TabClosingPolicy.UNAVAILABLE

        dock<MoveBackToCatalogEditor>()
    }


}
