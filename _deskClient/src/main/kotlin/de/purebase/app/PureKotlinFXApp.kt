package de.purebase.app

import javafx.application.Application
import javafx.application.Application.launch
import javafx.scene.Scene
import javafx.scene.control.Label
import javafx.scene.control.TextField
import javafx.scene.layout.VBox
import javafx.stage.Stage
import javafx.beans.property.SimpleStringProperty
import javafx.scene.control.Button
import javafx.scene.layout.HBox
import tornadofx.*


// https://github.com/MartinEden/Kotlin-JavaFX-Example/blob/master/src/main/kotlin/javafx/example/App.kt
// Note the the Gradle jfxRun task directly launches the JavaFxExample
// class, and bypasses this main function. This function exists so
// you can easily launch the application with a debugger attached.
fun main(args: Array<String>)
{
    launch(JavaFXExample::class.java)
}

class JavaFXExample : Application()
{
    
    override fun start(primaryStage: Stage)
    {
        val text1 = SimpleStringProperty().apply {
            setValue("test vorbelegung")
        }
        val layout = VBox().apply {
            children.add(Label("Hello, World!"))
            children.add(HBox().apply {
                children.add(TextField().apply {
                    this.textProperty().bind(text1)
                })
                children.add(Button().apply {
                    text("GO")
                })
            })
        }
        primaryStage.run {
            scene = Scene(layout)
            show()
        }
    }
}