package de.purebase.app.view

import de.purebase.util.PhotoFileCopyHelper
import javafx.application.Platform
import javafx.beans.property.SimpleStringProperty
import javafx.beans.value.ChangeListener
import javafx.collections.FXCollections
import javafx.scene.layout.HBox
import javafx.stage.DirectoryChooser
import tornadofx.*
import java.io.File
import kotlin.concurrent.thread

class PhotoFile(val id: Int, val path: String)

private val photoFiles = FXCollections.observableArrayList<PhotoFile>()

class MoveBackToCatalogEditor : View("MoveBackToCatalog") {

    val inFilePath = SimpleStringProperty().apply {
        setValue("d:\\")
    }

    val outFilePath = SimpleStringProperty().apply {
        setValue("e:\\")
    }

    init {
        println("MoveBackToCatalogEditor:init()")
        inFilePath.addListener(ChangeListener<String> { observable, oldValue, newValue ->
            run {
                System.out.printf("%s -> %s%n", oldValue, newValue)
                updatePhotoFileTable(newValue)
            }
        })
    }

    private fun updatePhotoFileTable(rootFolderPath: String) {
        resetPhotoFiles()
        val chosenFolder = File(rootFolderPath)
        if (chosenFolder.exists() && chosenFolder.isDirectory) {
            File(chosenFolder.path.toString()).walkTopDown().forEach {
                Platform.runLater {
                    addFileForDisplaying(it)
                }
            }
        } else {
            println("WARN: ${inFilePath.value} not exists or isn't an folder.")
        }
    }

    override val root = borderpane {
        left {
            vbox {
                label("right")

                // Nachfolgend 2 Varianten zum Einfuegen einer UI-Instanz:
                apply {
                    children.add(fileChooserInput("IN", inFilePath))
                }
                this += fileChooserInput("OUT", outFilePath)

                hbox {
                    button("START MOVE FILES") {
                        action {
                            thread {
                                val inputDir = File(inFilePath.value)
                                val outputDir = File(outFilePath.value)
                                val errors:List<String> = PhotoFileCopyHelper.movePhotoBackToArchivFolders(inputDir, outputDir, true)
                                // TODO Display the errors list.
                            }
                        }
                    }
                }
            }
        }
        center {
            useMaxWidth = true
            tableview<PhotoFile> {
                items = photoFiles
                readonlyColumn("ID", PhotoFile::id)
                readonlyColumn("Name", PhotoFile::path)
            }
        }
        right {
        }
    }

    private fun resetPhotoFiles() {
        photoFiles.clear()
    }

    private fun addFileForDisplaying(file: File) {
        val id = photoFiles.size
        photoFiles.add(PhotoFile(id, file.absolutePath))
    }

    private fun fileChooserInput(title: String, filePath: SimpleStringProperty): HBox {
        val root = HBox()
         with(root) {
            textfield {
                textProperty().bind(filePath)
                //setEditable(true)// funktioniert nur ohne binding
            }
            button(title) {
                action {
                    val preselectedDirectory = validatePreselection(filePath)
                    selectNewFolderByDialog(preselectedDirectory, filePath)

                }
            }
        }
        return root
    }

    private fun validatePreselection(filePath: SimpleStringProperty): File {
        var preselectedDirectory = File(filePath.value)
        if (!preselectedDirectory.canRead()) {
            preselectedDirectory = File(System.getProperty("user.home"))
        }
        return preselectedDirectory
    }

    private fun selectNewFolderByDialog(preselectedDirectory: File, filePath: SimpleStringProperty) {
        // https://stackoverflow.com/questions/14256588/opening-a-javafx-filechooser-in-the-user-directory
        val directoryChooser = DirectoryChooser()
        directoryChooser.initialDirectory = preselectedDirectory
        val chosenFolder = directoryChooser.showDialog(null)
        if (chosenFolder != null) filePath.set(chosenFolder.absolutePath)
    }

}