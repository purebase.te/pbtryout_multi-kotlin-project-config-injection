plugins {
    kotlin("jvm")
}

dependencies {
    implementation(project(":core"))
    implementation(kotlin("stdlib"))
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:1.0.1")

    // see https://github.com/beryx-gist/badass-jlink-example-kotlin-tornadofx/blob/master/build.gradle.kts
    implementation("no.tornado:tornadofx:1.7.17") {
        exclude("org.jetbrains.kotlin")
    }
}


