import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

buildscript {
    extra["kotlin_version"] = "1.4.10"
    extra["tornadofx_version"] = "1.7.17"
    //extra["junit_version"] = "5.6.2"

    repositories {
        mavenLocal()
        mavenCentral()
        jcenter()
    }

    dependencies {
        //val kotlinVersion2 = "1.4.10"
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${project.extra["kotlin_version"]}")
        classpath("org.junit.platform:junit-platform-gradle-plugin:1.1.0")
        classpath("no.tornado:tornadofx:${project.extra["tornadofx_version"]}")
    }
}

plugins {
    base
    // TODO wie project.extra["kotlin_version"] aufloesen?
    kotlin("jvm") version "1.4.10" apply false
    // kotlin("plugin.serialization") version "1.4.10"
}

allprojects {
    group = "de.purebase.app"
    version = "1.0"

    repositories {
        mavenLocal()
        mavenCentral()
        jcenter()
    }
}

// Configure all KotlinCompile tasks on each sub-project
subprojects {
    tasks.withType<KotlinCompile>().configureEach {
        println("Configuring $name in project ${project.name}...")
        kotlinOptions {
            suppressWarnings = true
            jvmTarget = "1.8"
        }
    }
}

dependencies {
    // Make the root project archives configuration depend on every subproject
    subprojects.forEach {
        // FIXME archives is deprecated. But what is the right option?
        archives(it)
    }
}
